## Tesseract

![](lupa.png)

Tesseract é um software de reconhecimento ótico de caracteres de código aberto, originalmente desenvolvido pela Hewlett-Packard e foi por um tempo mantido pelo Google; atualmente o projeto está hospedado no GitHub.

Se aplica a imagens em formato tiff com texto puro em uma única coluna, convertendo a saída em um arquivo txt. Não possui mecanismos para reconhecimento de layout, desta forma não é recomendável para textos que possuam imagens, fórmulas ou mais de uma coluna.

[Git-tesseract](https://gitlab.com/m-ab-s/tesseract)

Vídeo do tesseract:
{{<https://www.youtube.com/watch?v=3RdIRoUlVks>}}


## OpenCV

![](OpenCV.png) 

OpenCV ( Open Source Computer Vision Library ) é uma biblioteca de funções de programação voltada principalmente para a visão computacional em tempo real .  A biblioteca pode ser usada   para trabalhar com imagens ,seja pra ler, pra criar ou pra modificar.   A partir de 2011, o OpenCV apresenta aceleração de GPU para operações em tempo real.
 
[Git-OpenCV](https://github.com/opencv/opencv)

Vídeo do openCV:
{{<https://www.youtube.com/watch?v=m4Rl0EnCHyE>}}

Com o tesseract e o openCV é possível ler textos dentro de imagens.
Em python: 

(No windows é necessário instalar o tesseract em https://github.com/UB-Mannheim/tesseract/wiki)
​

import pytesseract

import cv2

#/ lê a imagem
#/ lê a imagem (imagem.png deve estar no mesmo local do código)

imagem = cv2.imread("imagem.png")

#/ local onde o tesseract está instalado 

local = r"C:\Program Files\Tesseract-OCR"

#/ extrai o texto da imagem

pytesseract.pytesseract.tesseract_cmd = local + r'\tesseract.exe'

texto = pytesseract.image_to_string(imagem)

#/ imprime o texto

print(texto)





## Hololens 2

![](hololens2.jpg)

Hololens 2 é um óculos de realidade mista ele faz aparecer na sua frente objetos tridimensionais e bidimensionais, podendo interagir com eles como se fossem reais, tudo em alta resolução. Os óculos reconhecem comandos de voz. Ele é conectado a internet e funciona como um computador autônomo.

Vídeo do hololens 2:
{{<https://www.youtube.com/watch?v=rMks7sMzPxI>}}


## Project Brooklyn

![](ProjectBrooklyn.jpg)

Projeto Brooklyn é uma cadeira de jogos conceitual projetada para redefinir o significado de imersão total em jogos. Está cadeira de última geração se transforma em uma potência de entretenimento com visuais panorâmicos em uma tela rollout(tela enrolável) de 60”, uma mesa dobrável com apoio de braço 4D modular e uma mesa transformável para jogos de PC e console.

Vídeo do Project Brooklyn:
{{<https://www.youtube.com/watch?v=MDqK_Wd-8i4>}}


## Drones

![](DroneDjiAgrasT20.png)

Os Drones atuais são capazes de realizar diversas tarefas com auxílio de softwares de última geração para operação e planejamento de missões, com muita confiabilidade e precisão, realizando algumas de suas funções de forma autônoma.
Toda essa tecnologia embarcada que grandes empresas do setor de construção, inspeção, segurança, agricultura entre outros, estão buscando integrar em seus processos, visando facilitar suas operações, reduzir tempo, custos, riscos e melhorando os resultados finais.

Vìdeo do Drone Dji Agras T20: 
{{<https://www.youtube.com/watch?v=hGyLjO7KWeU>}}



## O menor supercapacitor

![](Omenorsupercapacitor.png)

Ele tem o tamanho de uma partícula de poeira, tendo 0,001 milímetros cúbicos,
o que produz uma voltagem como uma bateria de dedo. Ele pode ser carregado por sangue humano ou por um eletrólito natural, isso abre novas possibilidades para operação de sensores médicos embutidos no corpo humano. Isso é um avanço no campo da medicina, quando for implementado, terá um grande impacto no diagnóstico e tratamento de várias doenças.

Mais informações sobre o menor supercapacitor:
[LINK](https://avalanchenoticias.com.br/nanotecnologia/um-supercapacitor-do-tamanho-de-uma-particula-de-poeira-foi-criado-o-que-produz-uma-voltagem-como-uma-bateria-de-dedo/)








